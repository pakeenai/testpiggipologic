function test1(value) {
    const input = value
    const number = input.toString()
    console.log("Test1 input :",number)
    var sum = true
    if(number.length < 6){
        sum = false
    }
    console.log("Test1 Sum :",sum)

}
function test2(value) {
    const input = value
    const number = input.toString()
    console.log("Test2 input :",number)
    var count = 0
    var sum = true
    for(var i = 0;i<number.length;i++){
        if(number[i+1] == number[i]){
            count++
            if(count > 1){
                sum = false
            }
        }else{
            count = 0
        }
    }
    console.log("Test2 Sum :",sum)

}
function test3(value) {
    const input = value
    const number = input.toString()
    var count1 = 0
    var count2 = 0
    var r = 0
    var sum = true
    console.log("Test3 input :",number)
    for(var i = 0;i<number.length;i++){
        r = number[i] - number[i+1]
        if(number[i] - number[i+1] >= 1){
            count1 = 0
            if(r == 1){
                count2++
            }else{
                count2 = 0
            }
            if(count2 > 1){
                sum = false
            }
            
        }else{
            if(r == -1){
                count1++
            }else{
                count1 = 0
            }
            if(count1 > 1){
                sum = false
            }
            
        }
    }
    console.log("Test3 Sum :",sum)
}
function test4(value) {
    const input = value
    const number = input.toString()
    console.log("Test4 input :",number)
    var sum = true
    var ar = []
    for(var i = 0;i<number.length;i++){
        if(i % 2 == 0){
            if(number[i] == number[i+1]){
                ar.push(number[i]+''+number[i+1])
            }
        }
    }
    if(ar.length > 2){
        sum = false
    }
    console.log("Test4 Sum :",sum)
}
console.log("----------- PASS -------------")
test1(172839)
test2(112762)
test3(124578)
test4(887712)
console.log("------------------------------")
console.log("----------- FALSE -------------")
test1(17283)
test2(111822)
test3(321895)
test4(112233)
console.log("-------------------------------")